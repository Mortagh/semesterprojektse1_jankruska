/**
 * @author jkrusk2s
 * Created by Temp on 06.01.2018.
 */


import org.junit.Before;
import org.junit.Test;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

public class PersistenceTest {
	@Before
	public void setUp(){ }

	@Test
	public void saveToXMLloadFromXML_emptyCarPark_equal(){
		assertEquals(MockUps.empty(),CarPark.fromXML(MockUps.empty().toXML()));
	}

	@Test
	public void saveToXMLloadFromXML_filledCarPark_equal(){
		assertEquals(MockUps.filled(),CarPark.fromXML(MockUps.filled().toXML()));
	}

	@Test
	public void saveToXMLloadFromXML_mockCarPark_1_equal(){
		assertEquals(MockUps.mockUp_1(),CarPark.fromXML(MockUps.mockUp_1().toXML()));
	}

	@Test
	public void saveToXMLloadFromXML_mockCarPark_2_equal(){
		assertEquals(MockUps.mockUp_2(),CarPark.fromXML(MockUps.mockUp_2().toXML()));
	}

	@Test
	public void saveToXMLloadFromXML_mockCarPark_3_equal(){
		assertEquals(MockUps.mockUp_3(),CarPark.fromXML(MockUps.mockUp_3().toXML()));
	}
}
