
/**
 * @author jkrusk2s
 * Created by Temp on 22.10.2017.
 */
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class SpacesInfoTest {
	CarPark filled;

	@Before
	public void setUp(){
		filled = new CarPark();
	}

	@Test
	public void emptySpaces_FilledCarPark_ReturnsZero(){
		assertEquals(filled.EmptySpaces(),0);
		//TODO: Test for different Vehicles
	}
}
