/**
 * @author jkrusk2s
 * Created by Temp on 06.01.2018.
 */
public class Controller {
	CarPark carPark;

	public Controller(CarPark carPark){
		this.carPark = carPark;
	}

	public Ticket add(Car car) throws Exception {
		Ticket ticket = new Ticket();//TODO: Ticket logic
		if(carPark.hasSpaces(car.getType())){
			ticket.setSpaceIndex(carPark.addCar(car));
			ticket.setCar(car);
			car.setTicket(ticket);

			carPark.addTicket(ticket);
			return ticket;
		}else{
			throw new Exception();//TODO: Decent Exception and/or Message to User
		}
	}


	public void remove(Car car){
		remove(car.getTicket());
	}

	public void remove(Ticket ticket){//TODO: Preis
		carPark.removeCar(carPark.removeTicket(ticket));
	}
}
