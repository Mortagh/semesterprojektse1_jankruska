

/**
 * @author jkrusk2s
 * Created by Temp on 06.01.2018.
 */
public class Ticket {
	private int id;
	private int spaceIndex;
	private Car car;
	private long date;

	public Ticket(){
		date = System.currentTimeMillis();//TODO: better Date handling
	}

	public void setSpaceIndex(int value){
		spaceIndex = value;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public long getDate(){
		return date;
	}

	public int getSpaceIndex() {
		return spaceIndex;
	}
}
