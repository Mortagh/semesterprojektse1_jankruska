/**
 * @author jkrusk2s
 * Created by Temp on 06.01.2018.
 */
public class Car {
	private int type;
	private Ticket ticket;

	public Car(){

	}

	public int getType(){
		return type;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Ticket getTicket() {
		return ticket;
	}
}
