/**
 * @author jkrusk2s
 * Created by Temp on 29.10.2017.
 */
public interface SpacesInfo {
	int EmptySpaces();
	int EmptySpaces(int type);
	double getFilledPercentage(int type);
}
