import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author jkrusk2s
 * Created by Temp on 29.10.2017.
 */
public class CarPark implements SpacesInfo {
	private Car[] spaces;
	private byte[] spaceType;
	private Map<Integer,String> spaceTypes;
	private int[] occupied; //Occupied spaces per type
	private int[] total;//Total Spaces per type
	private ArrayList<Ticket> tickets;

	public CarPark(){
		this(100);
	}

	public CarPark(int numberSpaces){
		this(numberSpaces,0);
	}

	public CarPark(int numberSpaces, int numberTypes){
		this(numberSpaces,numberTypes,null);
	}

	public CarPark(int numberSpaces, int numberTypes, ArrayList<String> typeNames){
		if (typeNames == null) {
			typeNames = new ArrayList<>();
			for (int i = 1; i <= numberTypes; i++) {
				typeNames.add("spaceType "+i);
			}
		}
		spaces = new Car[numberSpaces];
		spaceType = new byte[numberSpaces];
		spaceTypes.put(0,"All Vehicles");
		for (int i = 0; i < numberTypes; i++) {
			if (typeNames.get(i)==null)
			spaceTypes.put(i+1,typeNames.get(i));
		}
	}

	public int addCar(Car car){
		int spaceIndex = getParkingSpace(car.getType());
		spaces[spaceIndex] = car;
		occupied[car.getType()]++;
		return spaceIndex;
	}

	public Ticket addTicket(Ticket ticket){
		tickets.add(ticket);
		return ticket;
	}

	private int getParkingSpace(int type) {//TODO: More efficient way to get next free slot for a vehicle of spaceType than searching the whole array
		int i=0;
		while(spaces[i]!=null && this.spaceType[i]!=type){
			++i;
		}
		return i;
	}

	public boolean hasSpaces(int type){
		if (!spaceTypes.containsKey(type)){return false;}

		if (total[type]-occupied[type]>0){
			return true;
		}
		else {
			return false;
		}
	}

	public static CarPark fromXML(String xml){
		XStream xstream = new XStream();
		return (CarPark)xstream.fromXML(xml);
	}

	public static CarPark fromXML(File xml){
		XStream xstream = new XStream();
		return (CarPark)xstream.fromXML(xml);
	}

	public String toXML(){
		XStream xstream = new XStream();
		return xstream.toXML(this);
	}

	@Override
	public boolean equals(Object obj) {//TODO: Define equals
		if (obj==null || obj.getClass() != getClass()){
			return false;
		}
		return true;
	}

	@Override
	public int EmptySpaces() {//TODO: Gesamt freie Parkplät´ze
		return 0;
	}

	@Override
	public int EmptySpaces(int type) {
		if (!spaceTypes.containsKey(type)){
			return 0;
		}else{
			return total[type] - occupied[type];
		}
	}

	@Override
	public double getFilledPercentage(int type) {//TODO: Percentage stat
		return 0;
	}

	public int removeTicket(Ticket ticket) {
		tickets.remove(ticket);
		return ticket.getSpaceIndex();
	}

	public void removeCar(int index) {
		int type = spaces[index].getType();
		occupied[type]--;
		spaces[index]=null;
	}
}
